// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef XU_2024_EXAMPLE_2_PROPERTIES_HH
#define XU_2024_EXAMPLE_2_PROPERTIES_HH

#include <dune/alugrid/grid.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/boxdfm/model.hh>

#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>

#include "problem.hh"
#include "spatialparams.hh"
#include "spatialparams_facet.hh"
#include "spatialparams_equi.hh"

struct DummyCouplingManager {};

namespace Dumux::Properties {

// we need to derive first from the box-dfm Model and then the OneP TypeTag
// because the flux variables cache type of OneP is overwritten in BoxDfmModel
// Create new type tags
namespace TTag {
struct OnePIncompressible { using InheritsFrom = std::tuple<OneP>; };
struct OnePIncompressibleBox { using InheritsFrom = std::tuple<BoxModel, OnePIncompressible>; };
struct OnePIncompressibleBoxDfm { using InheritsFrom = std::tuple<BoxDfmModel, OnePIncompressible>; };
struct OnePIncompressibleBoxFacetBulk { using InheritsFrom = std::tuple<BoxFacetCouplingModel, OnePIncompressible>; };
struct OnePIncompressibleBoxFacetLowDim { using InheritsFrom = std::tuple<BoxModel, OnePIncompressible>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePIncompressibleBox> { using type = Dune::ALUGrid<2, 2, Dune::simplex , Dune::conforming>; };
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePIncompressibleBoxDfm> { using type = Dune::ALUGrid<2, 2, Dune::simplex , Dune::conforming>; };
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePIncompressibleBoxFacetBulk> { using type = Dune::ALUGrid<2, 2, Dune::simplex , Dune::conforming>; };
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePIncompressibleBoxFacetLowDim> { using type = Dune::FoamGrid<1, 2>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePIncompressible> { using type = OnePTestProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePIncompressibleBox>
{
    using type = OnePEquiDimensionalSpatialParams<
        GetPropType<TypeTag, Properties::GridGeometry>,
        GetPropType<TypeTag, Properties::Scalar>
    >;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePIncompressibleBoxDfm>
{
    using type = OnePTestSpatialParams<
        GetPropType<TypeTag, Properties::GridGeometry>,
        GetPropType<TypeTag, Properties::Scalar>
    >;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePIncompressibleBoxFacetBulk>
{
    using type = OnePFacetSpatialParams<
        GetPropType<TypeTag, Properties::GridGeometry>,
        GetPropType<TypeTag, Properties::Scalar>
    >;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePIncompressibleBoxFacetLowDim>
{
    using type = OnePFacetSpatialParams<
        GetPropType<TypeTag, Properties::GridGeometry>,
        GetPropType<TypeTag, Properties::Scalar>
    >;
};

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePIncompressible>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<0, Scalar>>;
};


template< class BulkTypeTag, class LowDimTypeTag >
class TestTraits
{
    using BulkFVGridGeometry = GetPropType<BulkTypeTag, Properties::GridGeometry>;
    using LowDimFVGridGeometry = GetPropType<LowDimTypeTag, Properties::GridGeometry>;
public:
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, LowDimTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingMapper<BulkFVGridGeometry, LowDimFVGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
};

using FacetTraits = TestTraits<TTag::OnePIncompressibleBoxFacetBulk, TTag::OnePIncompressibleBoxFacetLowDim>;
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePIncompressibleBoxFacetBulk>
{  using type = typename FacetTraits::CouplingManager;  };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePIncompressibleBoxFacetLowDim>
{  using type = typename FacetTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePIncompressibleBoxDfm>
{  using type = DummyCouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePIncompressibleBox>
{  using type = DummyCouplingManager; };

} // end namespace Dumux::Properties

#endif
