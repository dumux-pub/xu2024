Point(0) = {0., 0., 0.};
Point(1) = {1., 0., 0.};
Point(2) = {1., 0.2, 0.};
Point(3) = {1., 0.6, 0.};
Point(4) = {1., 1.0, 0.};
Point(5) = {0., 1.0, 0.};
Point(6) = {0., 0.8, 0.};
Point(7) = {0., 0.4, 0.};

Line(1) = {0, 1};
Line(2) = {1, 2};
Line(3) = {2, 3};
Line(4) = {3, 4};
Line(5) = {4, 5};
Line(6) = {5, 6};
Line(7) = {6, 7};
Line(8) = {7, 0};

Curve Loop(1) = {1:8};
Plane Surface(1) = {1};
Physical Surface(1) = {1};

Point(8) = {0.3, 0.2, 0.};
Point(9) = {0.7, 0.4, 0.};
Point(10) = {0.3, 0.6, 0.};
Point(11) = {0.7, 0.8, 0.};

Line(9) = {8, 2};
Line(10) = {7, 9};
Line(11) = {10, 3};
Line(12) = {6, 11};
Line{9:12} In Surface{1};
Physical Line(1) = {9:12};

Characteristic Length{:} = 0.01;
