SetFactory("OpenCASCADE");

a = 1e-3;
Rectangle(1) = {0, 0, 0, 1, 1, 0};
Rectangle(2) = {0.3, 0.2 - a/2.0, 0, 0.7, a, 0};
Rectangle(3) = {0.0, 0.4 - a/2.0, 0, 0.7, a, 0};
Rectangle(4) = {0.3, 0.6 - a/2.0, 0, 0.7, a, 0};
Rectangle(5) = {0.0, 0.8 - a/2.0, 0, 0.7, a, 0};

BooleanFragments{ Surface{1:5}; Delete; }{ Surface{1:5}; Delete; }

Characteristic Length{ PointsOf{Surface{6};} } = 0.1;
Field[1] = Distance;
Field[1].CurvesList = { Unique(Abs(Boundary{Surface{2:5};})) };
Field[1].Sampling = 150;

Field[2] = Threshold;
Field[2].InField = 1;
Field[2].SizeMin = a;
Field[2].SizeMax = 0.015;
Field[2].DistMin = a;
Field[2].DistMax = 0.1;

Field[3] = Constant;
Field[3].PointsList = {PointsOf{Surface{6};}};
Field[3].VIn = 0.1;
Field[3].VOut = 0.1;

Field[4] = Min;
Field[4].FieldsList = {2, 3};
Background Field = 4;

Physical Surface(3) = {3, 5};  // isotropic barriers
Physical Surface(1) = {6};     // background, created in fragments
Physical Surface(2) = {2, 4};  // anisotropic barriers
