// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef XU_2024_EXAMPLE_2_SPATIAL_PARAMS_EQUI_HH
#define XU_2024_EXAMPLE_2_SPATIAL_PARAMS_EQUI_HH

#include <dumux/discretization/method.hh>
#include <dumux/io/grid/griddata.hh>

#include <dumux/porousmediumflow/fvspatialparams1p.hh>

namespace Dumux {

template<class GridGeometry, class Scalar, bool isBoxDfm = true>
class OnePEquiDimensionalSpatialParams
: public FVPorousMediumFlowSpatialParamsOneP< GridGeometry, Scalar, OnePEquiDimensionalSpatialParams<GridGeometry, Scalar> >
{
    using ThisType = OnePEquiDimensionalSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsOneP<GridGeometry, Scalar, ThisType>;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridData = Dumux::GridData<typename GridView::Grid>;

    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    static constexpr bool isFracture = int(GridView::dimension) < int(GridView::dimensionworld);
    static constexpr Scalar invalidPermeability = -1.0;

public:
    using PermeabilityType = Dune::FieldMatrix<Scalar, 2, 2>;

    OnePEquiDimensionalSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry,
                                     std::shared_ptr<const GridData> gridData)
    : ParentType(gridGeometry)
    , permeability_(0.0)
    , permeabilityBarriers_(0.0)
    , permeabilityConduits_(0.0)
    , gridData_{gridData}
    {
        permeability_[0][0] = getParamFromGroup<Scalar>("Bulk", "SpatialParams.Permeability");
        permeability_[1][1] = getParamFromGroup<Scalar>("Bulk", "SpatialParams.Permeability");

        permeabilityBarriers_[0][0] = getParamFromGroup<Scalar>("Fracture", "SpatialParams.Permeability");
        permeabilityBarriers_[1][1] = getParamFromGroup<Scalar>("Fracture", "SpatialParams.Permeability");

        permeabilityConduits_[0][0] = getParamFromGroup<Scalar>("Fracture", "SpatialParams.TangentialPermeabilityConduits");
        permeabilityConduits_[1][1] = getParamFromGroup<Scalar>("Fracture", "SpatialParams.Permeability");
    }

    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume&,
                                  const ElementSolution&) const
    { return permeability(element); }

    PermeabilityType permeability(const Element& element) const
    {
        if (onConduit_(element))
            return permeabilityConduits_;
        if (onBarrier_(element))
            return permeabilityBarriers_;
        return permeability_;
    }

    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { return 1.0; }

    int marker(const Element& e) const
    { return gridData_->getElementDomainMarker(e); }

private:
    bool onBarrier_(const Element& e) const
    { return gridData_->getElementDomainMarker(e) == 3; }

    bool onConduit_(const Element& e) const
    { return gridData_->getElementDomainMarker(e) == 2; }

    PermeabilityType permeability_;
    PermeabilityType permeabilityBarriers_;
    PermeabilityType permeabilityConduits_;
    std::shared_ptr<const GridData> gridData_;
};

} // end namespace Dumux

#endif
