# SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
import shutil
import subprocess
import matplotlib.pyplot as plt
from numpy import genfromtxt

try:
    import seaborn as sns
    sns.set_theme()
except ImportError:
    print("'Seaborn' could not be found. Using default plot style.")

DUMUX_PLOT_SCRIPT = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "../../../../dumux/bin/postprocessing/extractlinedata.py"
)
BOX_DFM_FILE = "1pboxdfm-00001.vtu"
EBOX_DFM_FILE = "1peboxdfm_bulk-00001.vtu"
REFERENCE_FILE = "reference-00001.vtu"
EBOX_DFM_FRACTURE_FILE = "1peboxdfm_fracture-00001.vtp"

if not os.path.exists(DUMUX_PLOT_SCRIPT):
    exit(f"Could not find the script 'extractlinedata.py' at the expected location '{DUMUX_PLOT_SCRIPT}'")

def exit(msg: str) -> None:
    sys.stderr.write(msg)
    sys.exit(1)

def run(cmd: list[str], err_msg: str, shell: bool = False) -> None:
    try:
        subprocess.run(cmd, check=True)
    except Exception as e:
        exit(f"{e}\n{err_msg}")

def compile() -> None:
    run(["make", "example7"], f"Could not compile the executable for the box-dfm model")
    run(["make", "example7_equi"], f"Could not compile the executable for the box-dfm model")
    run(["make", "example7_ebox_dfm"], f"Could not compile the executable for the ebox-dfm model")

def run_simulations(caseb: bool = False, kt: float = 1.0) -> None:
    run(
        [
            "./example7_equi", "params.input", "-Grid.File", "grids/example7_equi.msh",
            "-Problem.IsCaseB", f"{caseb}", "-Fracture.SpatialParams.TangentialPermeabilityConduits", str(kt),
            "-Problem.Name", "reference"
        ],
        f"Simulation with the equi-dimensional box model was not successful"
    )
    run(
        ["./example7", "params.input",  "-Problem.IsCaseB", f"{caseb}", "-Fracture.SpatialParams.TangentialPermeabilityConduits", str(kt)],
        f"Simulation with the box-dfm model was not successful"
    )
    run(
        ["./example7_ebox_dfm", "params.input",  "-Problem.IsCaseB", f"{caseb}", "-Fracture.SpatialParams.TangentialPermeabilityConduits", str(kt)],
        f"Simulation with the ebox-dfm model was not successful"
    )

def create_plot_data(suffix: str = "") -> None:
    if not os.path.exists(BOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{BOX_DFM_FILE}'")
    if not os.path.exists(EBOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{EBOX_DFM_FILE}'")
    if not os.path.exists(REFERENCE_FILE):
        exit(f"Could not find expected reference results file '{REFERENCE_FILE}'")
    run([
            "pvpython", f"{DUMUX_PLOT_SCRIPT}",
            "-f", BOX_DFM_FILE, EBOX_DFM_FILE, REFERENCE_FILE,
            "-o", f"plot_data{suffix}", "--point1", "0.65", "0", "0", "--point2", "0.65", "1.0", "0", "-r", "1000"
        ],
        "Could not generate plot data using pvpython"
    )

def make_plots(suffix: str = "") -> None:
    box_dfm_file = f"plot_data{suffix}/{os.path.splitext(BOX_DFM_FILE)[0]}.csv"
    ebox_dfm_file = f"plot_data{suffix}/{os.path.splitext(EBOX_DFM_FILE)[0]}.csv"
    reference_file = f"plot_data{suffix}/{os.path.splitext(REFERENCE_FILE)[0]}.csv"
    if not os.path.exists(box_dfm_file):
        exit(f"Could not find expected box-dfm plot data file '{box_dfm_file}'")
    if not os.path.exists(ebox_dfm_file):
        exit(f"Could not find expected box-dfm plot data file '{ebox_dfm_file}'")
    if not os.path.exists(reference_file):
        exit(f"Could not find expected reference plot data file '{reference_file}'")
    plt.figure(1)
    box_dfm_data = genfromtxt(box_dfm_file, delimiter=",", names=True)
    ebox_dfm_data = genfromtxt(ebox_dfm_file, delimiter=",", names=True)
    reference_data = genfromtxt(reference_file, delimiter=",", names=True)
    plt.clf()
    plt.plot(reference_data["arc_length"], reference_data["p"], label="reference", color="k")
    plt.plot(box_dfm_data["arc_length"], box_dfm_data["p"], label="box-dfm")
    plt.plot(ebox_dfm_data["arc_length"], ebox_dfm_data["p"], label="ebox-dfm", linestyle="--")
    plt.xlabel("arc length")
    plt.ylabel("p")
    plt.legend()
    plt.savefig(f"example7_plot{suffix}.pdf", bbox_inches="tight")

def compute_diff(suffix: str = "") -> None:
    in_filename = BOX_DFM_FILE
    if suffix:
        stem, ext = os.path.splitext(in_filename)
        in_filename = f"{stem}{suffix}{ext}"
        shutil.copy(BOX_DFM_FILE, f"{stem}{suffix}{ext}")
        print(f"Copied vtu file to {in_filename}")
    subprocess.run(["fieldcompare", "file", in_filename, EBOX_DFM_FILE, "--diff", "--disable-mesh-reordering"], check=False)

def render_images(suffix: str = "") -> None:
    if not os.path.exists(REFERENCE_FILE):
        exit(f"Could not find expected reference results file '{REFERENCE_FILE}'")
    render_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), "pvstate.py")
    run(
        ["pvpython", render_file,
         "-f", REFERENCE_FILE, "-o", f"example7{suffix}.png", "--array", "p", "--label", "$p$"
        ],
        "Could not render image using pvpython"
    )
    print(f"Rendered p from {BOX_DFM_FILE}")

    diff_file = f"{os.path.splitext(BOX_DFM_FILE)[0]}{suffix}.vtu.vtu"
    if os.path.exists(diff_file):
        run(
            ["pvpython", render_file, "-f", diff_file, "-o", f"example7_delta_p{suffix}.png", "--array", "p", "--label", "$\\Delta p$"],
            "Could not render image using pvpython"
        )
        print(f"Rendered delta p from {diff_file}")


compile()

for k_t in [1e-3, 1, 1e3]:
    print(f"Running k_t = {k_t}")
    print("  -- Running case a")
    run_simulations(kt=k_t)
    create_plot_data(suffix=f"_kt_{k_t}")
    make_plots(suffix=f"_kt_{k_t}")
    compute_diff(suffix=f"_kt_{k_t}")
    render_images(suffix=f"_kt_{k_t}")

    print("  -- Running case b")
    run_simulations(caseb=True, kt=k_t)
    create_plot_data(suffix=f"_kt_{k_t}_case_b")
    make_plots(suffix=f"_kt_{k_t}_case_b")
    compute_diff(suffix=f"_kt_{k_t}_case_b")
    render_images(suffix=f"_kt_{k_t}_case_b")
