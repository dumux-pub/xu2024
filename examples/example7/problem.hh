// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef XU_2024_EXAMPLE_6_PROBLEM_HH
#define XU_2024_EXAMPLE_6_PROBLEM_HH

#include <cmath>
#include <algorithm>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/porousmediumflow/problem.hh>

struct DummyCouplingManager;

namespace Dumux {

template<class TypeTag>
class OnePTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;

    static constexpr Scalar eps_ = 1e-6;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr bool isFracture = int(GridView::dimension) < GridView::dimensionworld;

public:
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;

    OnePTestProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                    std::shared_ptr<SpatialParams> spatialParams,
                    const std::string& paramGroup = "")
    : ParentType(gridGeometry, spatialParams, paramGroup)
    , isCaseB_(getParamFromGroup<bool>("Problem", "IsCaseB"))
    {}

    template<class SubControlVolumeFace>
    BoundaryTypes interiorBoundaryTypes(const Element& e, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    // Source term (only for fracture domain in facet-coupling model)
    template<typename FVElementGeometry, typename ElementVolumeVariables, typename SubControlVolume>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        if constexpr (isFracture && !std::is_same_v<CouplingManager, DummyCouplingManager>)
        {
            auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
            source /= scv.volume()*elemVolVars[scv].extrusionFactor();
            return source;
        } else { return NumEqVector(0.0); }
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        if constexpr (isFracture) {
            values.setAllNeumann();
            if (isCaseB_ && (globalPos[0] < eps_ || globalPos[0] > 1.0 - eps_))
                values.setAllDirichlet();
        } else {
            values.setAllDirichlet();
            if (!isCaseB_ && (globalPos[0] < eps_ || globalPos[0] > 1.0 - eps_))
                values.setAllNeumann();
        }
        return values;
    }

    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        if (!isCaseB_ && globalPos[1] > 1.0 - eps_)
            return PrimaryVariables(1.0);
        return initialAtPos(globalPos);
    }

    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    { return NumEqVector(0.0); }

    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return isCaseB_ ? PrimaryVariables(dirichletFunctionCaseB_(globalPos)) : PrimaryVariables(0.0); }

    void setCouplingManager(std::shared_ptr<const CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    Scalar dirichletFunctionCaseB_(const GlobalPosition& globalPos) const
    {
        const auto x = globalPos[0];
        return (2*x - 1)*(3*x - 1);
    }

    std::shared_ptr<const CouplingManager> couplingManagerPtr_;
    bool isCaseB_;
};

} // end namespace Dumux

#endif
