// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef XU_2024_EXAMPLE_2_SPATIAL_PARAMS_FACET_HH
#define XU_2024_EXAMPLE_2_SPATIAL_PARAMS_FACET_HH

#include <dumux/discretization/method.hh>

#include <dumux/porousmediumflow/fvspatialparams1p.hh>

namespace Dumux {

template<class GridGeometry, class Scalar, bool isBoxDfm = true>
class OnePFacetSpatialParams
: public FVPorousMediumFlowSpatialParamsOneP< GridGeometry, Scalar, OnePFacetSpatialParams<GridGeometry, Scalar> >
{
    using ThisType = OnePFacetSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsOneP<GridGeometry, Scalar, ThisType>;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    static constexpr bool isFracture = int(GridView::dimension) < int(GridView::dimensionworld);
    static constexpr Scalar invalidPermeability = -1.0;

public:
    using PermeabilityType = Dune::FieldMatrix<Scalar, 2, 2>;

    OnePFacetSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry,
                           const std::string& paramGroup)
    : ParentType(gridGeometry)
    , permeability_(0.0)
    , permeabilityConduits_(0.0)
    , aperture_{getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture")}
    {
        permeability_[0][0] = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
        permeability_[1][1] = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");

        permeabilityConduits_[0][0] = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
        permeabilityConduits_[1][1] = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");

        if constexpr (isFracture)
            permeabilityConduits_[0][0] = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.TangentialPermeabilityConduits");
    }

    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    { return isFracture ? aperture_ : 1.0; }

    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        if constexpr (isFracture)
            if (onConduit_(scv.dofPosition()))
                return permeabilityConduits_;
        return permeability_;
    }

    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { return 1.0; }

private:
    bool onConduit_(const GlobalPosition& globalPos) const
    {
        const auto y = globalPos[1];
        return std::abs(y - 0.2) < 1e-6 || std::abs(y - 0.6) < 1e-6;
    }

    PermeabilityType permeability_;
    PermeabilityType permeabilityConduits_;
    Scalar aperture_;
};

} // end namespace Dumux

#endif
