mesh_size = 0.08;
Point(1) = {0., 0., 0., mesh_size};
Point(2) = {1.0, 0., 0., mesh_size};
Point(3) = {1.0, 1., 0., mesh_size};
Point(4) = {0.0, 1., 0., mesh_size};
Point(5) = {1.0, 0., 0., mesh_size};
Point(6) = {0.25, 0.75, 0.0, mesh_size};
Point(7) = {0.75, 0.25, 0., mesh_size};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line(5) = {6, 7};

Curve Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};
Line{5} In Surface{1};
Physical Line(1) = {5};
Physical Surface(1) = {1};

Characteristic Length{:} = mesh_size;
