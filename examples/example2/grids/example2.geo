mesh_size = 0.08;
Point(1) = {0., 0., 0., mesh_size};
Point(2) = {0.5, 0., 0., mesh_size};
Point(3) = {0.5, 0.5, 0., mesh_size};
Point(4) = {0.5, 1., 0., mesh_size};
Point(5) = {0.0, 1., 0., mesh_size};
Point(6) = {1.0, 0., 0., mesh_size};
Point(7) = {1.0, 1., 0., mesh_size};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 1};
Line(6) = {2, 6};
Line(7) = {6, 7};
Line(8) = {7, 4};

Curve Loop(1) = {1, 2, 3, 4, 5};
Curve Loop(2) = {6, 7, 8, -3, -2};
Plane Surface(1) = {1};
Plane Surface(2) = {2};
Physical Surface(1) = {1, 2};
Physical Line(1) = {3};

Characteristic Length{:} = mesh_size;
