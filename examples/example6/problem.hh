// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef XU_2024_EXAMPLE_5_PROBLEM_HH
#define XU_2024_EXAMPLE_5_PROBLEM_HH

#include <cmath>
#include <algorithm>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/discretization/evalgradients.hh>

#include <dumux/porousmediumflow/problem.hh>

struct DummyCouplingManager;

namespace Dumux {

template<class TypeTag>
class OnePTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;

    static constexpr Scalar eps_ = 1e-6;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr bool isFracture = int(GridView::dimension) < GridView::dimensionworld;

public:
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;

    OnePTestProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                    std::shared_ptr<SpatialParams> spatialParams,
                    const std::string& paramGroup = "")
    : ParentType(gridGeometry, spatialParams, paramGroup)
    {}

    template<class SubControlVolumeFace>
    BoundaryTypes interiorBoundaryTypes(const Element& e, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    // Source term (only for fracture domain in facet-coupling model)
    template<typename FVElementGeometry, typename ElementVolumeVariables, typename SubControlVolume>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        if constexpr (isFracture && !std::is_same_v<CouplingManager, DummyCouplingManager>)
        {
            auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
            source /= scv.volume()*elemVolVars[scv].extrusionFactor();
            return source;
        } else { return NumEqVector(0.0); }
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        if (isDirichletBoundaryPosition(globalPos))
            values.setAllDirichlet();
        return values;
    }

    bool isDirichletBoundaryPosition(const GlobalPosition& globalPos) const
    { return false; }

    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(1.0); }

    template<class FVElementGeometry, class ElementVolumeVariables, class ElemFluxVarsCache, class SubControlVolumeFace>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        const auto& globalPos = scvf.ipGlobal();
        if (std::all_of(globalPos.begin(), globalPos.end(), [] (auto x) { return x < 0.25 + eps_; }))
            return NumEqVector(-1.0);
        else if (isOnOutlet(globalPos)) {
            // modify element solution to carry outlet head and compute Robin-type flux
            auto elemSol = elementSolution(element, elemVolVars, fvGeometry);
            for (const auto& curScvf : scvfs(fvGeometry))
            {
                if (curScvf.boundary())
                {
                    const bool curOnOutlet = isOnOutlet(curScvf.ipGlobal());
                    if (curOnOutlet)
                    {
                        const auto diriValues = dirichletAtPos(curScvf.ipGlobal());
                        const auto& insideScv = fvGeometry.scv(curScvf.insideScvIdx());
                        elemSol[insideScv.localDofIndex()][0] = diriValues[0];
                    }
                }
            }

            // evaluate gradients using this element solution
            const auto gradHead = evalGradients(element,
                                                element.geometry(),
                                                fvGeometry.gridGeometry(),
                                                elemSol,
                                                scvf.ipGlobal())[0];

            // compute the flux
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];
            Scalar flux = gradHead * scvf.unitOuterNormal();
            flux *= -1.0 *volVars.permeability();
            flux *= volVars.density()*volVars.mobility();
            return PrimaryVariables(flux);
        }
        return NumEqVector(0.0);
    }

    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    void setCouplingManager(std::shared_ptr<const CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return std::all_of(globalPos.begin(), globalPos.end(), [] (auto x) { return x > 0.875 - eps_; }); }

private:
    std::shared_ptr<const CouplingManager> couplingManagerPtr_;
};

} // end namespace Dumux

#endif
