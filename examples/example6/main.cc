// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © Dennis Gläser <dennis.a.glaeser@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include <config.h>

#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/foamgrid/foamgrid.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/istl/matrixmarket.hh>

#include <dumux/common/initialize.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/multidomain/facet/gridmanager.hh>

#include <dumux/porousmediumflow/boxdfm/vtkoutputmodule.hh>
#include <dumux/porousmediumflow/boxdfm/fractureintersections.hh>

#include <dumux/porousmediumflow/boxdfm/assembler.hh>
#include <dumux/porousmediumflow/boxdfm/barrierfluxes.hh>

#include "properties.hh"

int main(int argc, char** argv)
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::OnePIncompressibleBoxDfm;

    // maybe initialize MPI and/or multithreading backend
    Dumux::initialize(argc, argv);
    const auto& mpiHelper = Dune::MPIHelper::instance();

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // we reuse the facet coupling grid manager to create the grid
    // from a mesh file with the fractures being incorporated as
    // lower-dimensional elements.
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using FractureGrid = Dune::FoamGrid<2, 3>;
    using GridManager = FacetCouplingGridManager<Grid, FractureGrid>;
    GridManager gridManager;
    gridManager.init();

    // matrix grid view is the first one (index 0) inside the manager
    const auto& leafGridView = gridManager.template grid<0>().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(
        leafGridView,
        BoxDfmFractureIntersections{Dune::Indices::_1, gridManager, [&] (auto&&...) { return true; }}
    );

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(
        gridGeometry,
        std::make_shared<typename Problem::SpatialParams>(gridGeometry)
    );

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(gridGeometry->numDofs());
    problem->applyInitialSolution(x);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // initialize the vtk output module
    using VtkOutputModule = BoxDfmVtkOutputModule<GridVariables, SolutionVector, FractureGrid>;
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    VtkOutputModule vtkWriter(*gridVariables, x, problem->name(), "", Dune::VTK::nonconforming);
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields
    vtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    const auto barrierNormalPermeability = getParam<double>("Fracture.SpatialParams.Permeability");
    const auto barrierAperture = getParamFromGroup<double>("Fracture", "SpatialParams.Aperture");
    BoxDfmImmiscibleBarrierFluxes barrierFluxes{
        *gridGeometry, *problem, [&] (auto&&...) {
            return BoxDfmBarrierProperties<double>{barrierAperture, barrierNormalPermeability};
        }
    };
    using Assembler = BoxDfmAssembler<TypeTag, decltype(barrierFluxes)>;
    auto assembler = std::make_shared<Assembler>(std::move(barrierFluxes), problem, gridGeometry, gridVariables);

    {
        std::ofstream matrixFile("example6_matrix_boxdfm_" + Dumux::getParam<std::string>("Matrix.FilenameSuffix") + ".mm", std::ios::out);
        std::ofstream rhsFile("example6_rhs_boxdfm_" + Dumux::getParam<std::string>("Matrix.FilenameSuffix") + ".mm", std::ios::out);
        std::ofstream diri_dof_file("example6_dirichlet_dofs_boxdfm_" + Dumux::getParam<std::string>("Matrix.FilenameSuffix") + ".mm", std::ios::out);
        matrixFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
        rhsFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
        assembler->assembleJacobianAndResidual(x);
        Dune::writeMatrixMarket(assembler->jacobian(), matrixFile);
        Dune::writeMatrixMarket(assembler->residual(), rhsFile);

        auto diri_dofs_mask = assembler->residual();
        diri_dofs_mask = 0.0;
        for (const auto& element : elements(gridGeometry->gridView())) {
            auto fvg = localView(*gridGeometry);
            fvg.bindElement(element);

            for (const auto& scvf : scvfs(fvg)) {
                const auto& scv = fvg.scv(scvf.insideScvIdx());
                if (scvf.boundary()) {
                    diri_dofs_mask[scv.dofIndex()] =
                        problem->isDirichletBoundaryPosition(scv.dofPosition()) ? 1 : 0;
                } else {
                    diri_dofs_mask[scv.dofIndex()] = 0;
                    const auto& outsideScv = fvg.scv(scvf.outsideScvIdx());
                    diri_dofs_mask[outsideScv.dofIndex()] = 0;
                }
            }
        }

        if (std::any_of(diri_dofs_mask.begin(), diri_dofs_mask.end(), [&] (auto k) { return k != 0.0; }))
            DUNE_THROW(Dune::InvalidStateException, "Unvisited dirichlet dof");

        Dune::writeMatrixMarket(diri_dofs_mask, diri_dof_file);
    }

    // the linear solver
    // using LinearSolver = ILUBiCGSTABIstlSolver<LinearSolverTraits<GridGeometry>, LinearAlgebraTraitsFromAssembler<Assembler>>;
    using LinearSolver = UMFPackIstlSolver<LinearSolverTraits<GridGeometry>, LinearAlgebraTraitsFromAssembler<Assembler>>;
    auto linearSolver = std::make_shared<LinearSolver>(); //gridGeometry->gridView(), gridGeometry->dofMapper());

    // solve the problem
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver(assembler, linearSolver).solve(x);

    // write the solution
    vtkWriter.write(1.0);

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
