# SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
import subprocess
import matplotlib.pyplot as plt
from numpy import genfromtxt

try:
    import seaborn as sns
    sns.set_theme()
except ImportError:
    print("'Seaborn' could not be found. Using default plot style.")

DUMUX_PLOT_SCRIPT = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "../../../../dumux/bin/postprocessing/extractlinedata.py"
)
BOX_DFM_FILE = "1pboxdfm-00001.vtu"
EBOX_DFM_FILE = "1peboxdfm_bulk-00001.vtu"
DIFF_FILE = "diff_1pboxdfm-00001.vtu.vtu"

if not os.path.exists(DUMUX_PLOT_SCRIPT):
    exit(f"Could not find the script 'extractlinedata.py' at the expected location '{DUMUX_PLOT_SCRIPT}'")

def exit(msg: str) -> None:
    sys.stderr.write(msg)
    sys.exit(1)

def run(cmd: list[str], err_msg: str, shell: bool = False) -> None:
    try:
        subprocess.run(cmd, check=True)
    except Exception as e:
        exit(f"{e}\n{err_msg}")

def compile() -> None:
    run(["make", "example6"], f"Could not compile the executable for the box-dfm model")
    run(["make", "example6_ebox_dfm"], f"Could not compile the executable for the ebox-dfm model")

def run_simulations() -> None:
    run(["./example6"], f"Simulation with the box-dfm model was not successful")
    run(["./example6_ebox_dfm"], f"Simulation with the ebox-dfm model was not successful")

def create_plot_data() -> None:
    if not os.path.exists(BOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{BOX_DFM_FILE}'")
    if not os.path.exists(EBOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{EBOX_DFM_FILE}'")
    run([
            "pvpython", f"{DUMUX_PLOT_SCRIPT}",
            "-f", BOX_DFM_FILE, EBOX_DFM_FILE,
            "-o", "plot_data", "--point1", "0", "0.0", "0", "--point2", "1", "1", "1", "-r", "1000"
        ],
        "Could not generate plot data using pvpython"
    )

def make_plots() -> None:
    box_dfm_file = f"plot_data/{os.path.splitext(BOX_DFM_FILE)[0]}.csv"
    ebox_dfm_file = f"plot_data/{os.path.splitext(EBOX_DFM_FILE)[0]}.csv"
    reference_file = f"reference_plot.csv"
    if not os.path.exists(box_dfm_file):
        exit(f"Could not find expected box-dfm plot data file '{box_dfm_file}'")
    if not os.path.exists(ebox_dfm_file):
        exit(f"Could not find expected box-dfm plot data file '{ebox_dfm_file}'")
    if not os.path.exists(reference_file):
        exit(f"Could not find expected reference data file '{reference_file}'")
    plt.figure(1)
    box_dfm_data = genfromtxt(box_dfm_file, delimiter=",", names=True)
    ebox_dfm_data = genfromtxt(ebox_dfm_file, delimiter=",", names=True)
    reference_data = genfromtxt(reference_file, delimiter=",")
    plt.plot(box_dfm_data["arc_length"], box_dfm_data["p"], label="box-dfm")
    plt.plot(ebox_dfm_data["arc_length"], ebox_dfm_data["p"], label="ebox-dfm", linestyle="--")
    plt.plot(reference_data[:, 0], reference_data[:, 1], label="reference", color="k")
    plt.xlabel("arc length")
    plt.ylabel("p")
    plt.legend()
    plt.savefig("example6_plot.pdf", bbox_inches="tight")

def compute_diff() -> None:
    subprocess.run(["fieldcompare", "file", BOX_DFM_FILE, EBOX_DFM_FILE, "--diff"], check=False)

def render_images() -> None:
    if not os.path.exists(BOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{BOX_DFM_FILE}'")
    render_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), "pvstate.py")
    run(
        ["pvpython", render_file, "-f", BOX_DFM_FILE, "-o", "example6.png", "--array", "p", "--label", "$p$", "--show-grid"],
        "Could not render image using pvpython"
    )
    print(f"Rendered p from {BOX_DFM_FILE}")
    if os.path.exists(DIFF_FILE):
        run(
            ["pvpython", render_file, "-f", DIFF_FILE, "-o", "example6_delta_p.png", "--array", "p", "--label", "$\\Delta p$"],
            "Could not render image using pvpython"
        )
        print(f"Rendered delta p from {DIFF_FILE}")


compile()
run_simulations()
create_plot_data()
make_plots()
compute_diff()
render_images()
