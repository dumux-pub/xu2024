# state file generated using paraview version 5.11.1
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

import os
CWD=os.path.abspath(os.getcwd())

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-o", "--out-filename", required=True, help="The name of the file where to save the screenshot")
parser.add_argument("-f", "--filename", required=True, help="The name of the file to render")
parser.add_argument("-a", "--array", required=True, help="The name of the array to render")
parser.add_argument("-l", "--label", required=False, help="The label to use for the array")
parser.add_argument("-g", "--show-grid", required=False, action="store_true", help="Use this flag to show the grid edges")
args = vars(parser.parse_args())

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [2294, 1382]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [0.5, 0.5, 0.0]
renderView1.OrientationAxesVisibility = 0
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-0.5432469565841014, -1.9959461882070664, 1.7068859802976855]
renderView1.CameraFocalPoint = [0.641292166257614, 0.7822592139534325, 0.26654080202601405]
renderView1.CameraViewUp = [0.2322433424687467, 0.3678042590980072, 0.9004349265040259]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.8660254037844386
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1994, 1382)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'XML Unstructured Grid Reader'
grid = XMLUnstructuredGridReader(
    registrationName=args["filename"],
    FileName=[os.path.join(os.path.dirname(os.path.abspath(__file__)), args["filename"])]
)
grid.CellArrayStatus = ['process rank']
grid.PointArrayStatus = ['p']
grid.TimeArray = 'None'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from grid
gridDisplay = Show(grid, renderView1, 'UnstructuredGridRepresentation')

# get 2D transfer function for the requested array
pTF2D = GetTransferFunction2D(args["array"])

# get color transfer function/color map for the requested array
pLUT = GetColorTransferFunction(args["array"])
pLUT.TransferFunction2D = pTF2D
pLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for the requested array
pPWF = GetOpacityTransferFunction(args["array"])
pPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
gridDisplay.Representation = 'Surface' if not args["show_grid"] else "Surface With Edges"
gridDisplay.ColorArrayName = ['POINTS', args["array"]]
gridDisplay.LookupTable = pLUT
gridDisplay.SelectTCoordArray = 'None'
gridDisplay.SelectNormalArray = 'None'
gridDisplay.SelectTangentArray = 'None'
gridDisplay.OSPRayScaleArray = args["array"]
gridDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
gridDisplay.SelectOrientationVectors = 'None'
gridDisplay.ScaleFactor = 0.1
gridDisplay.SelectScaleArray = args["array"]
gridDisplay.GlyphType = 'Arrow'
gridDisplay.GlyphTableIndexArray = args["array"]
gridDisplay.GaussianRadius = 0.005
gridDisplay.SetScaleArray = ['POINTS', args["array"]]
gridDisplay.ScaleTransferFunction = 'PiecewiseFunction'
gridDisplay.OpacityArray = ['POINTS', args["array"]]
gridDisplay.OpacityTransferFunction = 'PiecewiseFunction'
gridDisplay.DataAxesGrid = 'GridAxesRepresentation'
gridDisplay.PolarAxes = 'PolarAxesRepresentation'
gridDisplay.ScalarOpacityFunction = pPWF
gridDisplay.ScalarOpacityUnitDistance = 0.04954539839319495
gridDisplay.OpacityArrayName = ['POINTS', args["array"]]
gridDisplay.SelectInputVectors = [None, '']
gridDisplay.WriteLog = ''
gridDisplay.RescaleTransferFunctionToDataRange(False, True)

# setup the color legend parameters for each legend in this view

# get color legend/bar for pLUT in view renderView1
pLUTColorBar = GetScalarBar(pLUT, renderView1)
pLUTColorBar.WindowLocation = 'Any Location'
pLUTColorBar.Position = [0.826303400174368, 0.3470622286541245]
pLUTColorBar.Title = args["label"] if "label" in args else args["array"]
pLUTColorBar.ComponentTitle = ''
pLUTColorBar.TitleJustification = 'Left'
pLUTColorBar.HorizontalTitle = 1
pLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
pLUTColorBar.TitleFontSize = 55
pLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
pLUTColorBar.LabelFontSize = 45
pLUTColorBar.ScalarBarThickness = 33
pLUTColorBar.ScalarBarLength = 0.4
pLUTColorBar.AutomaticLabelFormat = 0
pLUTColorBar.LabelFormat = '%-#6.1e'
pLUTColorBar.DrawDataRange = 0

# set color bar visibility
pLUTColorBar.Visibility = 1

# show color legend
gridDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(gridDisplay)
# ----------------------------------------------------------------

SaveScreenshot(args["out_filename"], renderView1, TransparentBackground=True)

if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
