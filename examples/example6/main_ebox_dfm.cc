// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © Dennis Gläser <dennis.a.glaeser@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include <config.h>

#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/foamgrid/foamgrid.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/istl/matrixmarket.hh>

#include <dumux/common/initialize.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/porousmediumflow/boxdfm/vtkoutputmodule.hh>
#include <dumux/porousmediumflow/boxdfm/fractureintersections.hh>

#include <dumux/porousmediumflow/boxdfm/assembler.hh>
#include <dumux/porousmediumflow/boxdfm/barrierfluxes.hh>

#include "properties.hh"

template<typename M>
auto toBCRSMatrix(const M& multiTypeMatrix) {
    return Dumux::MatrixConverter<M>::multiTypeToBCRSMatrix(multiTypeMatrix);
}

template<typename V>
auto toBCRSVector(const V& multiTypeVector) {
    return Dumux::VectorConverter<V>::multiTypeToBlockVector(multiTypeVector);
}

int main(int argc, char** argv)
{
    using namespace Dumux;

    // maybe initialize MPI and/or multithreading backend
    Dumux::initialize(argc, argv);
    const auto& mpiHelper = Dune::MPIHelper::instance();

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    using BulkProblemTypeTag = Properties::TTag::OnePIncompressibleBoxFacetBulk;
    using LowDimProblemTypeTag = Properties::TTag::OnePIncompressibleBoxFacetLowDim;
    using BulkGrid = GetPropType<BulkProblemTypeTag, Properties::Grid>;
    using LowDimGrid = GetPropType<LowDimProblemTypeTag, Properties::Grid>;

    using GridManager = FacetCouplingGridManager<BulkGrid, LowDimGrid>;
    GridManager gridManager;
    gridManager.init();
    gridManager.loadBalance();

    // we compute on the leaf grid views
    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& lowDimGridView = gridManager.template grid<1>().leafGridView();

    // create the finite volume grid geometries
    using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings>;
    using BulkFVGridGeometry = GetPropType<BulkProblemTypeTag, Properties::GridGeometry>;
    using LowDimFVGridGeometry = GetPropType<LowDimProblemTypeTag, Properties::GridGeometry>;
    BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
    auto bulkFvGridGeometry = std::make_shared<BulkFVGridGeometry>(bulkGridView, lowDimGridView, facetGridAdapter);
    auto lowDimFvGridGeometry = std::make_shared<LowDimFVGridGeometry>(lowDimGridView);

    // the coupling mapper
    using TestTraits = Properties::TestTraits<BulkProblemTypeTag, LowDimProblemTypeTag>;
    auto couplingMapper = std::make_shared<typename TestTraits::CouplingMapper>();
    couplingMapper->update(*bulkFvGridGeometry, *lowDimFvGridGeometry, gridManager.getEmbeddings());

    // the coupling manager
    using CouplingManager = typename TestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    using BulkProblem = GetPropType<BulkProblemTypeTag, Properties::Problem>;
    using LowDimProblem = GetPropType<LowDimProblemTypeTag, Properties::Problem>;
    auto bulkSpatialParams = std::make_shared<typename BulkProblem::SpatialParams>(bulkFvGridGeometry, "Bulk");
    auto bulkProblem = std::make_shared<BulkProblem>(bulkFvGridGeometry, bulkSpatialParams, "Bulk");
    auto lowDimSpatialParams = std::make_shared<typename LowDimProblem::SpatialParams>(lowDimFvGridGeometry, "Fracture");
    auto lowDimProblem = std::make_shared<LowDimProblem>(lowDimFvGridGeometry, lowDimSpatialParams, "Fracture");
    bulkProblem->setCouplingManager(couplingManager);
    lowDimProblem->setCouplingManager(couplingManager);

    // the solution vector
    using MDTraits = typename TestTraits::MDTraits;
    using SolutionVector = typename MDTraits::SolutionVector;
    SolutionVector x;

    static const auto bulkId = typename MDTraits::template SubDomain<0>::Index();
    static const auto lowDimId = typename MDTraits::template SubDomain<1>::Index();
    x[bulkId].resize(bulkFvGridGeometry->numDofs());
    x[lowDimId].resize(lowDimFvGridGeometry->numDofs());
    bulkProblem->applyInitialSolution(x[bulkId]);
    lowDimProblem->applyInitialSolution(x[lowDimId]);

    // initialize coupling manager
    couplingManager->init(bulkProblem, lowDimProblem, couplingMapper, x);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkProblemTypeTag, Properties::GridVariables>;
    using LowDimGridVariables = GetPropType<LowDimProblemTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkFvGridGeometry);
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimProblem, lowDimFvGridGeometry);
    bulkGridVariables->init(x[bulkId]);
    lowDimGridVariables->init(x[lowDimId]);

    // initialize the vtk output module
    using BulkSolutionVector = std::decay_t<decltype(x[bulkId])>;
    using LowDimSolutionVector = std::decay_t<decltype(x[lowDimId])>;
    VtkOutputModule<BulkGridVariables, BulkSolutionVector> bulkVtkWriter(
        *bulkGridVariables, x[bulkId], bulkProblem->name(), "Bulk", Dune::VTK::nonconforming
    );
    VtkOutputModule<LowDimGridVariables, LowDimSolutionVector> lowDimVtkWriter(*lowDimGridVariables, x[lowDimId], lowDimProblem->name(), "LowDim");
    lowDimVtkWriter.addVolumeVariable([] (const auto& vv) { return vv.permeability(); }, "K");

    // Add model specific output fields
    using BulkIOFields = GetPropType<BulkProblemTypeTag, Properties::IOFields>;
    using LowDimIOFields = GetPropType<LowDimProblemTypeTag, Properties::IOFields>;
    BulkIOFields::initOutputModule(bulkVtkWriter);
    LowDimIOFields::initOutputModule(lowDimVtkWriter);

    // write initial solution
    bulkVtkWriter.write(0.0);
    lowDimVtkWriter.write(0.0);

    // the assembler
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(bulkProblem, lowDimProblem),
                                                  std::make_tuple(bulkFvGridGeometry, lowDimFvGridGeometry),
                                                  std::make_tuple(bulkGridVariables, lowDimGridVariables),
                                                  couplingManager);

    {
        std::ofstream matrixFile("example6_matrix_ebox_dfm_" + Dumux::getParam<std::string>("Matrix.FilenameSuffix") + ".mm", std::ios::out);
        std::ofstream rhsFile("example6_rhs_ebox_dfm_" + Dumux::getParam<std::string>("Matrix.FilenameSuffix") + ".mm", std::ios::out);
        std::ofstream diri_dof_file("example6_dirichlet_dofs_eboxdfm_" + Dumux::getParam<std::string>("Matrix.FilenameSuffix") + ".mm", std::ios::out);
        matrixFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
        rhsFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
        assembler->assembleJacobianAndResidual(x);
        Dune::writeMatrixMarket(toBCRSMatrix(assembler->jacobian()), matrixFile);
        Dune::writeMatrixMarket(toBCRSVector(assembler->residual()), rhsFile);

        auto diri_dofs_mask = assembler->residual();
        diri_dofs_mask = 42.0;
        for (const auto& element : elements(bulkFvGridGeometry->gridView())) {
            auto fvg = localView(*bulkFvGridGeometry);
            fvg.bindElement(element);

            for (const auto& scvf : scvfs(fvg)) {
                const auto& scv = fvg.scv(scvf.insideScvIdx());
                if (scvf.boundary()) {
                    if (bulkProblem->isDirichletBoundaryPosition(scv.dofPosition()))
                        diri_dofs_mask[Dune::index_constant<0>{}][scv.dofIndex()] = 1;
                    else
                        diri_dofs_mask[Dune::index_constant<0>{}][scv.dofIndex()] = 0;
                } else {
                    diri_dofs_mask[Dune::index_constant<0>{}][scv.dofIndex()] = 0;
                    diri_dofs_mask[Dune::index_constant<0>{}][fvg.scv(scvf.outsideScvIdx()).dofIndex()] = 0;
                }
            }
        }
        for (const auto& element : elements(lowDimFvGridGeometry->gridView())) {
            auto fvg = localView(*lowDimFvGridGeometry);
            fvg.bindElement(element);

            for (const auto& scvf : scvfs(fvg)) {
                const auto& scv = fvg.scv(scvf.insideScvIdx());
                if (scvf.boundary()) {
                    if (bulkProblem->isDirichletBoundaryPosition(scv.dofPosition()))
                        diri_dofs_mask[Dune::index_constant<1>{}][scv.dofIndex()] = 1;
                    else
                        diri_dofs_mask[Dune::index_constant<1>{}][scv.dofIndex()] = 0;
                } else {
                    diri_dofs_mask[Dune::index_constant<1>{}][scv.dofIndex()] = 0;
                    diri_dofs_mask[Dune::index_constant<1>{}][fvg.scv(scvf.outsideScvIdx()).dofIndex()] = 0;
                }
            }
        }

        if (std::any_of(diri_dofs_mask[Dune::index_constant<0>{}].begin(), diri_dofs_mask[Dune::index_constant<0>{}].end(), [&] (auto k) { return k != 0.0; }))
            DUNE_THROW(Dune::InvalidStateException, "Unexpected bulk dirichlet dof");
        if (std::any_of(diri_dofs_mask[Dune::index_constant<1>{}].begin(), diri_dofs_mask[Dune::index_constant<1>{}].end(), [&] (auto k) { return k != 0.0; }))
            DUNE_THROW(Dune::InvalidStateException, "Unvisited fracture dirichlet dof");

        Dune::writeMatrixMarket(toBCRSVector(diri_dofs_mask), diri_dof_file);
    }

    // the linear solver
    // using LinearSolver = ILUBiCGSTABIstlSolver<SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<Assembler>>;
    using LinearSolver = UMFPackIstlSolver<SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<Assembler>>;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    // linearize & solve
    newtonSolver->solve(x);

    // update grid variables for output
    bulkGridVariables->update(x[bulkId]);
    lowDimGridVariables->update(x[lowDimId]);

    // write vtk output
    bulkVtkWriter.write(1.0);
    lowDimVtkWriter.write(1.0);

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
