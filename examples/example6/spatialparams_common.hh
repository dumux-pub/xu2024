// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef XU_2024_EXAMPLE_5_SPATIAL_PARAMS_COMMON_HH
#define XU_2024_EXAMPLE_5_SPATIAL_PARAMS_COMMON_HH

namespace Dumux::Xu2024::Example5 {

template<typename GlobalPosition>
bool isInOmegaLow1(const GlobalPosition& globalPos)
{ return globalPos[0] > 0.5 && globalPos[1] < 0.5; }

template<typename GlobalPosition>
bool isInOmegaLow2(const GlobalPosition& globalPos)
{ return globalPos[0] > 0.75 && globalPos[1] < 0.75 && globalPos[1] > 0.5 && globalPos[2] > 0.5; }

template<typename GlobalPosition>
bool isInOmegaLow3(const GlobalPosition& globalPos)
{ return globalPos[0] < 0.75 && globalPos[0] > 0.625 && globalPos[1] < 0.625 && globalPos[1] > 0.5 && globalPos[2] > 0.5 && globalPos[2] < 0.75; }

template<typename GlobalPosition>
bool isInLowPermRegion(const GlobalPosition& globalPos)
{ return isInOmegaLow1(globalPos) || isInOmegaLow2(globalPos) || isInOmegaLow3(globalPos); }

template<typename GlobalPosition, typename K>
K matrixPermeability(const GlobalPosition& globalPos, K permeability)
{ return isInLowPermRegion(globalPos) ? permeability/10.0 : permeability; }

} // end namespace Dumux::Xu2024::Example5

#endif
