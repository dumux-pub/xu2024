// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef XU_2024_EXAMPLE_1_PROPERTIES_HH
#define XU_2024_EXAMPLE_1_PROPERTIES_HH

#include <dune/alugrid/grid.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/boxdfm/model.hh>

#include "problem.hh"
#include "spatialparams.hh"

namespace Dumux::Properties {

// we need to derive first from the box-dfm Model and then the OneP TypeTag
// because the flux variables cache type of OneP is overwritten in BoxDfmModel
// Create new type tags
namespace TTag {
struct OnePIncompressibleBoxDfm { using InheritsFrom = std::tuple<BoxDfmModel, OneP>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePIncompressibleBoxDfm> { using type = Dune::ALUGrid<2, 2, Dune::simplex , Dune::conforming>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePIncompressibleBoxDfm> { using type = OnePTestProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePIncompressibleBoxDfm>
{
private:
    using FVG = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = OnePTestSpatialParams<FVG, Scalar>;
};

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePIncompressibleBoxDfm>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<0, Scalar>>;
};

// Caching preferences
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::OnePIncompressibleBoxDfm> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::OnePIncompressibleBoxDfm> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::OnePIncompressibleBoxDfm> { static constexpr bool value = false; };

} // end namespace Dumux::Properties

#endif
