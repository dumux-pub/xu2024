import sys
import math
import subprocess

if len(sys.argv) < 2:
    sys.stderr.write("Expected a runtime argument: number of refinements\n")
    sys.exit(1)

errors = []
num_refinements = int(sys.argv[1])
subprocess.run(["make", "example1"], check=True)
subprocess.run(["cp", "grids/example1.msh", "grids/example1.tmp.msh"], check=True)
for ref in range(num_refinements + 1):
    print(f"Running refinement {ref}")
    if ref > 0:
        print(" - refining mesh")
        subprocess.run(["gmsh", "-refine", "-format", "msh2", "-v", "0", "grids/example1.tmp.msh"])

    print(" - running simulation")
    sim_output = subprocess.run(
        ["./example1", "params.input", "-Grid.File", "grids/example1.tmp.msh"],
        check=True, text=True, capture_output=True
    ).stdout

    errors.append(float(sim_output.split("L2 error =")[1].split("\n")[0].strip()))
    print(f" - L2 error: {errors[-1]}")

print("L2 Error table:")
print("refinement | error    | rate")

rates_str = []
for i in range(num_refinements + 1):
    rate = -1.0*(math.log(errors[i]) - math.log(errors[i-1]))/math.log(2.0) if i > 0 else 0.0
    rate_str = f"{rate:.2f}" if i > 0 else "-"
    rates_str.append(rate_str)
    print(f"{i:<10} | {errors[i]:.2e} | {rate_str}")

with open("table.tex", "w") as tex_table:
    tex_table.write(r"""
\begin{table}[!hbpt]
\centering
\begin{tabular}{c c c}
\hline\hline
$i$ & $||p_h-p||_{2}$ & Order \\ \hline\hline
""")
    for i, (error, rate_str) in enumerate(zip(errors, rates_str)):
        tex_table.write(f"""
{i} & {error:.2e} & {rate_str} \\\\ \\hline""")
    tex_table.write(r"""
\end{tabular}
\caption{Convergence test in Example \ref{ex:convergence}. Errors are evaluated on $\Omega^{\pm}$.}\label{tab:convergence}
\end{table}
""")
