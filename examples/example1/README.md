# Example 1

To run this example (convergence test), simply type

```
python3 convergencetest.py N
```

into the terminal, where `N` is the number of desired refinements. Note that this example requires [gmsh](https://gmsh.info) for the construction
and refinement of the meshes.
