mesh_size = 0.1;
Point(1) = {0., 0., 0., mesh_size};
Point(2) = {0.5, 0., 0., mesh_size};
Point(3) = {0.5, 1., 0., mesh_size};
Point(4) = {0.0, 1., 0., mesh_size};
Point(5) = {1.0, 0., 0., mesh_size};
Point(6) = {1.0, 1., 0., mesh_size};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line(5) = {2, 5};
Line(6) = {5, 6};
Line(7) = {6, 3};

Curve Loop(1) = {1, 2, 3, 4};
Curve Loop(2) = {5, 6, 7, -2};
Plane Surface(1) = {1};
Plane Surface(2) = {2};
Physical Surface(1) = {1, 2};
Physical Line(1) = {2};
