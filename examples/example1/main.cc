// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup TwoPTests
 * \brief Test for the two-phase porous medium flow model.
 */
#include <config.h>

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/foamgrid/foamgrid.hh>
#include <dune/geometry/quadraturerules.hh>

#include <dumux/common/initialize.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/multidomain/facet/gridmanager.hh>

#include <dumux/porousmediumflow/boxdfm/vtkoutputmodule.hh>
#include <dumux/porousmediumflow/boxdfm/fractureintersections.hh>

#include <dumux/porousmediumflow/boxdfm/assembler.hh>
#include <dumux/porousmediumflow/boxdfm/barrierfluxes.hh>

#include "properties.hh"

int main(int argc, char** argv)
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::OnePIncompressibleBoxDfm;

    // maybe initialize MPI and/or multithreading backend
    Dumux::initialize(argc, argv);
    const auto& mpiHelper = Dune::MPIHelper::instance();

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // we reuse the facet coupling grid manager to create the grid
    // from a mesh file with the fractures being incorporated as
    // lower-dimensional elements.
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using FractureGrid = Dune::FoamGrid<1, 2>;
    using GridManager = FacetCouplingGridManager<Grid, FractureGrid>;
    GridManager gridManager;
    gridManager.init();

    // matrix grid view is the first one (index 0) inside the manager
    const auto& leafGridView = gridManager.template grid<0>().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(
        leafGridView,
        BoxDfmFractureIntersections{Dune::Indices::_1, gridManager, [] (auto&&...) { return true; }}
    );

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(gridGeometry->numDofs());
    problem->applyInitialSolution(x);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // initialize the vtk output module
    using VtkOutputModule = BoxDfmVtkOutputModule<GridVariables, SolutionVector, FractureGrid>;
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    VtkOutputModule vtkWriter(*gridVariables, x, problem->name(), "", Dune::VTK::nonconforming);
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields
    vtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    const auto barrierNormalPermeability = getParam<double>("SpatialParams.BarrierNormalPermeability");
    const auto barrierAperture = getParam<double>("SpatialParams.BarrierAperture");
    BoxDfmImmiscibleBarrierFluxes barrierFluxes{
        *gridGeometry, *problem, [&] (auto&&...) {
            return BoxDfmBarrierProperties<double>{barrierAperture, barrierNormalPermeability};
        }
    };
    using Assembler = BoxDfmAssembler<TypeTag, decltype(barrierFluxes)>;
    auto assembler = std::make_shared<Assembler>(std::move(barrierFluxes), problem, gridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = ILUBiCGSTABIstlSolver<LinearSolverTraits<GridGeometry>, LinearAlgebraTraitsFromAssembler<Assembler>>;
    auto linearSolver = std::make_shared<LinearSolver>(gridGeometry->gridView(), gridGeometry->dofMapper());

    // solve the problem
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver(assembler, linearSolver).solve(x);

    // write the solution
    vtkWriter.write(1.0);

    // compute error
    double error = 0.0;
    for (const auto& element : elements(gridGeometry->gridView()))
    {
        std::vector<Dune::FieldVector<double, 1>> shapeValues;
        const auto fvGeometry = localView(*gridGeometry).bindElement(element);
        const auto interpolate = [&] (const auto& ipLocal) {
            fvGeometry.feLocalBasis().evaluateFunction(ipLocal, shapeValues);
            double result = 0.0;
            for (const auto& scv : scvs(fvGeometry))
                if (!scv.isOnFracture())
                    result += shapeValues[scv.localDofIndex()]*x[scv.dofIndex()][0];
            return result;
        };

        const auto& rule = Dune::QuadratureRules<double, 2>::rule(element.type(), /*order*/2);
        for (const auto& ip : rule)
        {
            const auto discrete = interpolate(ip.position());
            const auto exact = problem->exactSolution(element, element.geometry().global(ip.position()));
            const auto delta = discrete - exact;
            error += delta*delta*ip.weight()*element.geometry().integrationElement(ip.position());
        }
    }

    using std::sqrt;
    std::cout << "L2 error = " << sqrt(error) << std::endl;

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
