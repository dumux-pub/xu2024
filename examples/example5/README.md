# Example 5

To compile and run this example, type

```
make example5
./example5
```

into your terminal. This should produce the files `1pboxdfm-00001.vtu` (solution in the bulk matrix) and `1pboxdfm_fracture-00001.vtp` (solution on the fractures), which you can visualize with [ParaView](https://paraview.org). If [fieldcompare](https://pypi.org/project/fieldcompare/) is found in your
python environment, the difference between the solutions obtained with the `box-dfm` and the `ebox-dfm` schemes is computed and also visualized.

There are three different grids provided in the folder `grids`. To select them, you may set the parameter `Grid.File` in the `params.input` file accordingly.

The following result files are written in a simulation:

- `*.vtu` / `*.vtp` files containing the numerical solutions
- `*.png` files with visualizations of the results
- `*.pdf` files with generated plots
- folders named `plot_data*` with `*.csv` files from which the plots were generated
