# SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
import subprocess
import matplotlib.pyplot as plt
from numpy import genfromtxt

try:
    import seaborn as sns
    sns.set_theme()
except ImportError:
    print("'Seaborn' could not be found. Using default plot style.")

DUMUX_PLOT_SCRIPT = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "../../../../dumux/bin/postprocessing/extractlinedata.py"
)
BOX_DFM_FILE = "1pboxdfm-00001.vtu"
EBOX_DFM_FILE = "1peboxdfm_bulk-00001.vtu"
REFERENCE_FILE = "reference_bulk-00001.vtu"
EBOX_DFM_FRACTURE_FILE = "1peboxdfm_fracture-00001.vtp"
DIFF_FILE = "diff_1pboxdfm-00001.vtu.vtu"

if not os.path.exists(DUMUX_PLOT_SCRIPT):
    exit(f"Could not find the script 'extractlinedata.py' at the expected location '{DUMUX_PLOT_SCRIPT}'")

def exit(msg: str) -> None:
    sys.stderr.write(msg)
    sys.exit(1)

def run(cmd: list[str], err_msg: str, shell: bool = False) -> None:
    try:
        subprocess.run(cmd, check=True)
    except Exception as e:
        exit(f"{e}\n{err_msg}")

def compile() -> None:
    run(["make", "example5"], f"Could not compile the executable for the box-dfm model")
    run(["make", "example5_ebox_dfm"], f"Could not compile the executable for the ebox-dfm model")

def run_simulations() -> None:
    run([
        "./example5_ebox_dfm", "params.input",
        "-Grid.File", "grids/example5_reference.msh",
        "-Bulk.Problem.Name", "reference_bulk",
        "-Fracture.Problem.Name", "reference_fracture"
    ], f"Reference simulation was not successful")
    run(["./example5"], f"Simulation with the box-dfm model was not successful")
    run(["./example5_ebox_dfm"], f"Simulation with the ebox-dfm model was not successful")

def create_plot_data() -> None:
    if not os.path.exists(BOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{BOX_DFM_FILE}'")
    if not os.path.exists(EBOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{EBOX_DFM_FILE}'")
    run([
            "pvpython", f"{DUMUX_PLOT_SCRIPT}",
            "-f", BOX_DFM_FILE, EBOX_DFM_FILE, REFERENCE_FILE,
            "-o", "plot_data", "--point1", "0", "0", "0", "--point2", "700.0", "600.0", "0", "-r", "1000"
        ],
        "Could not generate plot data using pvpython"
    )
    run([
            "pvpython", f"{DUMUX_PLOT_SCRIPT}",
            "-f", BOX_DFM_FILE, EBOX_DFM_FILE, REFERENCE_FILE,
            "-o", "plot_data_2", "--point1", "625", "0", "0", "--point2", "625.0", "600.0", "0", "-r", "1000"
        ],
        "Could not generate plot data using pvpython"
    )

def make_plots(suffix: str = "") -> None:
    box_dfm_file = f"plot_data{suffix}/{os.path.splitext(BOX_DFM_FILE)[0]}.csv"
    ebox_dfm_file = f"plot_data{suffix}/{os.path.splitext(EBOX_DFM_FILE)[0]}.csv"
    reference_file = f"plot_data{suffix}/{os.path.splitext(REFERENCE_FILE)[0]}.csv"
    if not os.path.exists(box_dfm_file):
        exit(f"Could not find expected box-dfm plot data file '{box_dfm_file}'")
    if not os.path.exists(ebox_dfm_file):
        exit(f"Could not find expected box-dfm plot data file '{ebox_dfm_file}'")
    if not os.path.exists(reference_file):
        exit(f"Could not find reference plot data file '{reference_file}'")
    plt.figure(1)
    box_dfm_data = genfromtxt(box_dfm_file, delimiter=",", names=True)
    ebox_dfm_data = genfromtxt(ebox_dfm_file, delimiter=",", names=True)
    reference_data = genfromtxt(reference_file, delimiter=",", names=True)
    plt.clf()
    plt.plot(reference_data["arc_length"], reference_data["p"], label="reference", color="k")
    plt.plot(box_dfm_data["arc_length"], box_dfm_data["p"], label="box-dfm")
    plt.plot(ebox_dfm_data["arc_length"], ebox_dfm_data["p"], label="ebox-dfm", linestyle="--")
    plt.xlabel("arc length")
    plt.ylabel("p")
    plt.legend()
    plt.savefig(f"example5_plot{suffix}.pdf", bbox_inches="tight")

def compute_diff() -> None:
    subprocess.run(["fieldcompare", "file", BOX_DFM_FILE, EBOX_DFM_FILE, "--diff"], check=False)

def render_images() -> None:
    if not os.path.exists(BOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{BOX_DFM_FILE}'")
    render_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), "pvstate.py")
    run(
        ["pvpython", render_file,
         "-f", BOX_DFM_FILE, "-o", "example5.png", "--array", "p", "--label", "$p$", "--show-grid",
         "--show-grid", "--fracture-grid", EBOX_DFM_FRACTURE_FILE, "--tube-radius", "1"
        ],
        "Could not render image using pvpython"
    )
    print(f"Rendered p from {BOX_DFM_FILE}")
    if os.path.exists(DIFF_FILE):
        run(
            ["pvpython", render_file, "-f", DIFF_FILE, "-o", "example5_delta_p.png", "--array", "p", "--label", "$\\Delta p$"],
            "Could not render image using pvpython"
        )
        print(f"Rendered delta p from {DIFF_FILE}")


compile()
run_simulations()
create_plot_data()
make_plots()
make_plots(suffix="_2")
compute_diff()
render_images()
