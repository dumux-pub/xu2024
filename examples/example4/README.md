# Example 4

To compile and run this example, type

```
make example4
./example4
```

into your terminal. This should produce the files `1pboxdfm-00001.vtu` (solution in the bulk matrix) and `1pboxdfm_fracture-00001.vtp` (solution on the fractures), which you can visualize with [ParaView](https://paraview.org). To create all the plots, you may type

```
python3 run_and_plot.py
```

into your terminal. Note, however, that this requires `pvpython` (comes with `ParaView`) to postprocess the simulation results, and [matplotlib](https://matplotlib.org/) to create the plots. If [fieldcompare](https://pypi.org/project/fieldcompare/) is found in your
python environment, the difference between the solutions obtained with the `box-dfm` and the `ebox-dfm` schemes is computed and also visualized.

Note that there are two cases implemented: _flow from top to bottom_ and _flow from left to right_. To switch between these you can use the parameter `Problem.FlowFromLeftToRight` in the `params.input` file (set it to `true` or `false`).

The following result files are written in a simulation:

- `*.vtu` / `*.vtp` files containing the numerical solutions
- `*.png` files with visualizations of the results
- `*.pdf` files with generated plots
- a folder `plot_data` with `*.csv` files from which the plots were generated
- `*.mm` file(s) containing the system matrix in matrix market format (e.g. for analysis in Matlab)
