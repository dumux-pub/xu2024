# SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
import subprocess
import argparse
import matplotlib.pyplot as plt
from numpy import genfromtxt

try:
    import seaborn as sns
    sns.set_theme()
except ImportError:
    print("'Seaborn' could not be found. Using default plot style.")

DUMUX_PLOT_SCRIPT = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "../../../../dumux/bin/postprocessing/extractlinedata.py"
)
BOX_DFM_FILE = "1pboxdfm-00001.vtu"
EBOX_DFM_FILE = "1peboxdfm_bulk-00001.vtu"
EBOX_DFM_FRACTURE_FILE = "1peboxdfm_fracture-00001.vtp"
DIFF_FILE = "diff_1pboxdfm-00001.vtu.vtu"

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--with-mpfa", required=False, action="store_true")
args = vars(parser.parse_args())

if not os.path.exists(DUMUX_PLOT_SCRIPT):
    exit(f"Could not find the script 'extractlinedata.py' at the expected location '{DUMUX_PLOT_SCRIPT}'")

def exit(msg: str) -> None:
    sys.stderr.write(msg)
    sys.exit(1)

def run(cmd: list[str], err_msg: str, shell: bool = False) -> None:
    try:
        subprocess.run(cmd, check=True)
    except Exception as e:
        exit(f"{e}\n{err_msg}")

def compile() -> None:
    run(["make", "example4"], f"Could not compile the executable for the box-dfm model")
    run(["make", "example4_ebox_dfm"], f"Could not compile the executable for the ebox-dfm model")
    if args["with_mpfa"]:
        run(["make", "example4_mpfa_dfm"], f"Could not compile the executable for the ebox-dfm model")

def run_simulations() -> None:
    run(["./example4"], f"Simulation with the box-dfm model was not successful")
    run(["./example4_ebox_dfm"], f"Simulation with the ebox-dfm model was not successful")
    if args["with_mpfa"]:
        run(
            ["./example4_mpfa_dfm", "params.input", "-Bulk.Problem.Name", "1pmpfadfm_bulk", "-Fracture.Problem.Name", "1pmpfadfm_fracture"],
            f"Simulation with the ebox-dfm model was not successful"
        )

def is_top_to_bottom() -> bool:
    for line in open("params.input").readlines():
        if "FlowFromLeftToRight" in line:
            return line.split("FlowFromLeftToRight")[1].strip().split("=")[1].strip().split("#")[0].lower() in ["false", "0"]
    return False

def create_plot_data() -> None:
    reference_file = "reference_top_to_bottom.vtu" if is_top_to_bottom() else "reference.vtu"
    if not os.path.exists(BOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{BOX_DFM_FILE}'")
    if not os.path.exists(EBOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{EBOX_DFM_FILE}'")
    if not os.path.exists(reference_file):
        exit(f"Could not find reference results file '{reference_file}'")
    files = [BOX_DFM_FILE, EBOX_DFM_FILE, reference_file]
    if args["with_mpfa"]:
        files.append("1pmpfadfm_bulk-00001.vtu")
    run([
            "pvpython", f"{DUMUX_PLOT_SCRIPT}",
            "-f", *files,
            "-o", "plot_data", "--point1", "0", "0.5", "0", "--point2", "1.0", "0.9", "0", "-r", "1000"
        ],
        "Could not generate plot data using pvpython"
    )

def make_plots() -> None:
    box_dfm_file = f"plot_data/{os.path.splitext(BOX_DFM_FILE)[0]}.csv"
    ebox_dfm_file = f"plot_data/{os.path.splitext(EBOX_DFM_FILE)[0]}.csv"
    mpfa_dfm_file = f"plot_data/1pmpfadfm_bulk-00001.csv"
    reference_file = f"plot_data/reference_top_to_bottom.csv" if is_top_to_bottom() else f"plot_data/reference.csv"
    if not os.path.exists(box_dfm_file):
        exit(f"Could not find expected box-dfm plot data file '{box_dfm_file}'")
    if not os.path.exists(ebox_dfm_file):
        exit(f"Could not find expected box-dfm plot data file '{ebox_dfm_file}'")
    plt.figure(1)
    box_dfm_data = genfromtxt(box_dfm_file, delimiter=",", names=True)
    ebox_dfm_data = genfromtxt(ebox_dfm_file, delimiter=",", names=True)
    reference_data = genfromtxt(reference_file, delimiter=",", names=True)
    if args["with_mpfa"]:
        mpfa_data = genfromtxt(mpfa_dfm_file, delimiter=",", names=True)
    plt.plot(reference_data["arc_length"], reference_data["pressure"], label="reference", color="k")
    plt.plot(box_dfm_data["arc_length"], box_dfm_data["p"], label="box-dfm")
    plt.plot(ebox_dfm_data["arc_length"], ebox_dfm_data["p"], label="ebox-dfm", linestyle="--")
    if args["with_mpfa"]:
        plt.plot(mpfa_data["arc_length"], mpfa_data["p"], label="mpfa", linestyle="-.")
    plt.xlabel("arc length")
    plt.ylabel("p")
    plt.legend()
    plt.savefig("example4_plot.pdf", bbox_inches="tight")

def compute_diff() -> None:
    # yields ebox - boxdfm
    subprocess.run(["fieldcompare", "file", BOX_DFM_FILE, EBOX_DFM_FILE, "--diff"], check=False)

def render_images() -> None:
    if not os.path.exists(BOX_DFM_FILE):
        exit(f"Could not find expected box-dfm results file '{BOX_DFM_FILE}'")
    render_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), "pvstate.py")
    run(
        ["pvpython", render_file,
         "-f", BOX_DFM_FILE, "-o", "example4.png", "--array", "p", "--label", "$p$",
         "--show-grid", "--fracture-grid", EBOX_DFM_FRACTURE_FILE, "--tube-radius", "0.002"
         ],
        "Could not render image using pvpython"
    )
    print(f"Rendered p from {BOX_DFM_FILE}")
    if os.path.exists(DIFF_FILE):
        run(
            ["pvpython", render_file, "-f", DIFF_FILE, "-o", "example4_delta_p.png", "--array", "p", "--label", "$\\Delta p$"],
            "Could not render image using pvpython"
        )
        print(f"Rendered delta p from {DIFF_FILE}")


compile()
run_simulations()
create_plot_data()
make_plots()
compute_diff()
render_images()
