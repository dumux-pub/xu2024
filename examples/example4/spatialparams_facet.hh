// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef XU_2024_EXAMPLE_2_SPATIAL_PARAMS_FACET_HH
#define XU_2024_EXAMPLE_2_SPATIAL_PARAMS_FACET_HH

#include <dumux/discretization/method.hh>
#include <dumux/io/grid/griddata.hh>

#include <dumux/porousmediumflow/fvspatialparams1p.hh>

namespace Dumux {

template<class GridGeometry, class Scalar>
class OnePFacetSpatialParams
: public FVPorousMediumFlowSpatialParamsOneP< GridGeometry, Scalar, OnePFacetSpatialParams<GridGeometry, Scalar> >
{
    using ThisType = OnePFacetSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsOneP<GridGeometry, Scalar, ThisType>;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridData = Dumux::GridData<typename GridView::Grid>;

    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    static constexpr Scalar invalidPermeability = -1.0;
    static constexpr bool isFracture = int(GridView::dimension) < int(GridView::dimensionworld);

public:
    using PermeabilityType = Scalar;

    OnePFacetSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry,
                           std::shared_ptr<const GridData> gridData,
                           const std::string& paramGroup)
    : ParentType(gridGeometry)
    , permeability_{getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability")}
    , barrierPermeability_{getParamFromGroup<Scalar>(paramGroup, "SpatialParams.BarrierPermeability", invalidPermeability)}
    , aperture_{getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture")}
    , gridData_{gridData}
    {}

    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    { return isFracture ? aperture_ : 1.0; }

    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        if (isFracture && getElementDomainMarker(element) == 2)
        {
            if (barrierPermeability_ == invalidPermeability)
                DUNE_THROW(Dune::InvalidStateException,
                           "No barrier permeability defined! This should not be called for the bulk domain?");
            return barrierPermeability_;
        }
        else
            return permeability_;
    }

    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { return 1.0; }

    auto getElementDomainMarker(const Element& e) const
    { return gridData_->getElementDomainMarker(e); }

private:
    Scalar permeability_;
    Scalar barrierPermeability_;
    Scalar aperture_;
    std::shared_ptr<const GridData> gridData_;
};

} // end namespace Dumux

#endif
