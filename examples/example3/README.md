# Example 3

To compile and run this example, type

```
make example3
./example3
```

into your terminal. This should produce the files `1pboxdfm-00001.vtu` (solution in the bulk matrix) and `1pboxdfm_fracture-00001.vtp` (solution on the fractures), which you can visualize with [ParaView](https://paraview.org). To create all the plots, you may type

```
python3 run_and_plot.py
```

into your terminal. Note, however, that this requires `pvpython` (comes with `ParaView`) to postprocess the simulation results, and [matplotlib](https://matplotlib.org/) to create the plots. If [fieldcompare](https://pypi.org/project/fieldcompare/) is found in your
python environment, the difference between the solutions obtained with the `box-dfm` and the `ebox-dfm` schemes is computed and also visualized.

There are three different grids provided in the folder `grids`. To select them, you may set the parameter `Grid.File` in the `params.input` file accordingly.

The following result files are written in a simulation:

- `*.vtu` / `*.vtp` files containing the numerical solutions
- `*.png` files with visualizations of the results
- `*.pdf` files with generated plots
- a folder `plot_data` with `*.csv` files from which the plots were generated
- `*.mm` file(s) containing the system matrix in matrix market format (e.g. for analysis in Matlab)
