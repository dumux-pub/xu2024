# SPDX-FileCopyrightInfo: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

dune_symlink_to_source_files(FILES "params.input" "grids" "run_and_plot.py" "pvstate.py" "reference.vtu")

dumux_add_test(NAME example3
              SOURCES main.cc
              CMAKE_GUARD dune-alugrid_FOUND
              CMAKE_GUARD dune-foamgrid_FOUND)

dumux_add_test(NAME example3_ebox_dfm
              SOURCES main_ebox_dfm.cc
              CMAKE_GUARD dune-alugrid_FOUND
              CMAKE_GUARD dune-foamgrid_FOUND)
