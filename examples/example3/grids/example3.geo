// Gmsh geometry specification for the Geiger example
// hybrid-dimensional

DefineConstant [ lc = 0.1 ];
lcf = lc;

Point(1) = {0, 0, 0, lc};
Point(2) = {1, 0, 0, lc};
Point(3) = {1, 1, 0, lc};
Point(4) = {0, 1, 0, lc};
Point(5) = {0.5, 0, 0, lcf};
Point(6) = {1, 0.5, 0, lcf};
Point(7) = {0.5, 1, 0, lcf};
Point(8) = {0, 0.5, 0, lcf};
Point(9) = {0.75, 0.5, 0, lcf};
Point(10) = {1, 0.75, 0, lcf};
Point(11) = {0.75, 1, 0, lcf};
Point(12) = {0.5, 0.75, 0, lcf};
Point(13) = {0.625, 0.5, 0, lcf};
Point(14) = {0.75, 0.625, 0, lcf};
Point(15) = {0.625, 0.75, 0, lcf};
Point(16) = {0.5, 0.625, 0, lcf};
Point(17) = {0.5, 0.5, 0, lcf};
Point(18) = {0.625, 0.625, 0, lcf};
Point(19) = {0.75, 0.75, 0, lcf};

// boundary
Line(1) = {1, 5};
Line(2) = {2, 5};
Line(3) = {2, 6};
Line(4) = {6, 10};
Line(5) = {3, 10};
Line(6) = {3, 11};
Line(7) = {7, 11};
Line(8) = {4, 7};
Line(9) = {4, 8};
Line(10) = {1, 8};

// fractures
Line(11) = {13, 17};
Line(12) = {9, 13};
Line(13) = {6, 9};
Line(14) = {7, 12};
Line(15) = {12, 16};
Line(16) = {16, 17};
Line(17) = {9, 14};
Line(18) = {14, 19};
Line(19) = {11, 19};
Line(20) = {13, 18};
Line(21) = {15, 18};
Line(22) = {16, 18};
Line(23) = {14, 18};
Line(24) = {12, 15};
Line(25) = {15, 19};
Line(26) = {10, 19};
Line(27) = {5, 17};
Line(28) = {8, 17};

Line Loop(1) = {1, 27, -28, -10};
Line Loop(2) = {-2, 3, 13, 12, 11, -27};
Line Loop(3) = {28, -16, -15, -14, -8, 9};
Line Loop(4) = {-13, 4, 26, -18, -17};
Line Loop(5) = {-26, -5, 6, 19};
Line Loop(6) = {24, 25, -19, -7, 14};
Line Loop(7) = {-11, 20, -22, 16};
Line Loop(8) = {-12, 17, 23, -20};
Line Loop(9) = {22, -21, -24, 15};
Line Loop(10) = {-23, 18, -25, 21};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};
Plane Surface(6) = {6};
Plane Surface(7) = {7};
Plane Surface(8) = {8};
Plane Surface(9) = {9};
Plane Surface(10) = {10};

Physical Surface(0) = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
Physical Line(1) = {11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28};
