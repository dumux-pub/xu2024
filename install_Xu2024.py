#!/usr/bin/env python3

#
# This installs the module Xu2024 and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
#
#
# |      module name      |   branch name   |                 commit sha                 |         commit date         |
# |-----------------------|-----------------|--------------------------------------------|-----------------------------|
# |      dune-alugrid     |  origin/master  |  5e13b40d16e153103236772206ba246cd1723c8d  |  2023-09-07 16:07:56 +0200  |
# |       dune-istl       |  origin/master  |  f0fe9b6c2628b8520b59de59b9c5bd421bde0653  |  2023-09-20 14:03:48 +0000  |
# |       dune-grid       |  origin/master  |  765d9fe42b2d42846fbfa971804f952c6bfd39bc  |  2023-09-22 15:53:23 +0000  |
# |  dune-localfunctions  |  origin/master  |  a400a4dd9112a45001d130bda6e7ab36fdd41e3e  |  2023-08-30 14:24:55 +0000  |
# |     dune-geometry     |  origin/master  |  5712c0629ba958ed028db9e8f36cd2ae9b3e5e49  |  2023-06-30 15:58:55 +0000  |
# |      dune-subgrid     |  origin/master  |  e83f3f919c2602425467ed767f279bc9c356c436  |  2023-01-04 19:24:06 +0000  |
# |     dune-functions    |  origin/master  |  e1a4c1796441822e2cecb3f18a6d713cdeef959c  |  2023-09-15 20:29:20 +0000  |
# |     dune-foamgrid     |  origin/master  |  4845fbcac1bb814fc65173dfd2f4d1d58ce33863  |  2023-02-02 11:36:13 +0000  |
# |      dune-uggrid      |  origin/master  |  68094a7154287db0b10ffe07667d175cdd3eacff  |  2023-04-15 20:14:51 +0000  |
# |      dune-common      |  origin/master  |  5094a692f04e7ca2e8cc17602ac30c4b485ea2df  |  2023-09-22 23:14:55 +0000  |
# |     dune-typetree     |  origin/master  |  9b6217b147b274cb442257d37ae9985d5470364d  |  2023-07-14 10:30:00 +0000  |
# |        dumux          |  feature/box-dfm-barriers  |  46aa4dcd9d0a69a9c76a5614c0d327f8ed88c086  |                             |

import os
import sys
import subprocess

top = "."
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.split("/")[-1]
    if targetFolder.endswith(".git"):
        targetFolder = targetFolder[:-4]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dune-alugrid")
installModule("dune-alugrid", "https://gitlab.dune-project.org/extensions/dune-alugrid.git", "origin/master", "5e13b40d16e153103236772206ba246cd1723c8d", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/master", "f0fe9b6c2628b8520b59de59b9c5bd421bde0653", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/master", "765d9fe42b2d42846fbfa971804f952c6bfd39bc", )

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/master", "a400a4dd9112a45001d130bda6e7ab36fdd41e3e", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/master", "5712c0629ba958ed028db9e8f36cd2ae9b3e5e49", )

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://gitlab.dune-project.org/extensions/dune-subgrid.git", "origin/master", "e83f3f919c2602425467ed767f279bc9c356c436", )

print("Installing dune-functions")
installModule("dune-functions", "https://gitlab.dune-project.org/staging/dune-functions.git", "origin/master", "e1a4c1796441822e2cecb3f18a6d713cdeef959c", )

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "origin/master", "4845fbcac1bb814fc65173dfd2f4d1d58ce33863", )

print("Installing dune-uggrid")
installModule("dune-uggrid", "https://gitlab.dune-project.org//staging/dune-uggrid.git", "origin/master", "68094a7154287db0b10ffe07667d175cdd3eacff", )

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/master", "5094a692f04e7ca2e8cc17602ac30c4b485ea2df", )

print("Installing dune-typetree")
installModule("dune-typetree", "https://gitlab.dune-project.org/staging/dune-typetree.git", "origin/master", "9b6217b147b274cb442257d37ae9985d5470364d", )

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "feature/box-dfm-barriers", "46aa4dcd9d0a69a9c76a5614c0d327f8ed88c086")

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=Xu2024/cmake.opts', 'all'],
    '.'
)
