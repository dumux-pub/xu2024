## Xu2024

This repository contains the numerical examples of the paper:

> TODO


## License

This project is licensed under the terms and conditions of the GNU General Public
License (GPL) version 3 or - at your option - any later version.
The GPL can be found under [GPL-3.0-or-later.txt](LICENSES/GPL-3.0-or-later.txt)
provided in the `LICENSES` directory located at the topmost of the source code tree.


## Version Information

|      module name      |   branch name   |                 commit sha                 |         commit date         |
|-----------------------|-----------------|--------------------------------------------|-----------------------------|
|      dune-alugrid     |  origin/master  |  5e13b40d16e153103236772206ba246cd1723c8d  |  2023-09-07 16:07:56 +0200  |
|       dune-istl       |  origin/master  |  f0fe9b6c2628b8520b59de59b9c5bd421bde0653  |  2023-09-20 14:03:48 +0000  |
|       dune-grid       |  origin/master  |  765d9fe42b2d42846fbfa971804f952c6bfd39bc  |  2023-09-22 15:53:23 +0000  |
|  dune-localfunctions  |  origin/master  |  a400a4dd9112a45001d130bda6e7ab36fdd41e3e  |  2023-08-30 14:24:55 +0000  |
|     dune-geometry     |  origin/master  |  5712c0629ba958ed028db9e8f36cd2ae9b3e5e49  |  2023-06-30 15:58:55 +0000  |
|      dune-subgrid     |  origin/master  |  e83f3f919c2602425467ed767f279bc9c356c436  |  2023-01-04 19:24:06 +0000  |
|     dune-functions    |  origin/master  |  e1a4c1796441822e2cecb3f18a6d713cdeef959c  |  2023-09-15 20:29:20 +0000  |
|     dune-foamgrid     |  origin/master  |  4845fbcac1bb814fc65173dfd2f4d1d58ce33863  |  2023-02-02 11:36:13 +0000  |
|      dune-uggrid      |  origin/master  |  68094a7154287db0b10ffe07667d175cdd3eacff  |  2023-04-15 20:14:51 +0000  |
|      dune-common      |  origin/master  |  5094a692f04e7ca2e8cc17602ac30c4b485ea2df  |  2023-09-22 23:14:55 +0000  |
|     dune-typetree     |  origin/master  |  9b6217b147b274cb442257d37ae9985d5470364d  |  2023-07-14 10:30:00 +0000  |
TODO: mention dumux

## Installation

For a successful installation, you need to have the following software installed:

- fairly recent c/c++ compilers (e.g. `gcc>=9`)
- [cmake](https://cmake.org)
- the direct solver `umfpack` (on ubuntu, you can install it via `apt install libsuitesparse-dev`)
- [ParaView](https://paraview.org) for visualization of results

The installation procedure is done as follows:
Create a root folder, e.g. `DUMUX`, enter the previously created folder,
clone this repository and use the install script `install_Xu2024.py`
provided in this repository to install all dependent modules.

```sh
mkdir DUMUX
cd DUMUX
git clone https://git.iws.uni-stuttgart.de/dumux-pub/xu2024.git Xu2024
./Xu2024/install_Xu2024.py
```

This will clone all modules into the directory `DUMUX`, configure your module with `dunecontrol`.
Afterwards, you can head to the build folder of an example (e.g. `build-cmake/examples/example1`) to build and execute it.
Further instructions can be found in the `README` files of the examples (e.g. `examples/example1/README.md`).
